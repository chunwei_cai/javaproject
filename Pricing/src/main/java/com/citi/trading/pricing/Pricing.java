package com.citi.trading.pricing;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Client component for the pricing service. Holds a publish/subscribe registry
 * so that many subscribers can be notified of pricing data on a given stock,
 * based on a single request to the remote service. When configured as a Spring bean,
 * this component will make HTTP requests on a 15-second timer using 
 * Spring scheduling (if enabled).
 * 
 * Requires a property that provides the URL of the remote service. 
 * 
 * @author Will Provost
 *
 */

interface Subject {
	public void registerObserver(Observer o);
	public void unregisterObserver(Observer o);
	public void notifyObservers();
}

interface Observer {
	public void update(ArrayList<PricePoint> pricesSent);
}

public class Pricing {
	
	private class StockDate implements Subject {
		String quote;  //maybe a list 
		ArrayList<PricePoint> pricesSent; 
		ArrayList<Observer> observerList;
		
		public StockDate() {
			observerList = new ArrayList<Observer>();
		}

		@Override
		public void registerObserver(Observer o) {
			// TODO Auto-generated method stub
			observerList.add(o);
		}

		@Override
		public void unregisterObserver(Observer o) {
			// TODO Auto-generated method stub
			observerList.remove(observerList.indexOf(o));
		}

		@Override
		public void notifyObservers() {
			// TODO Auto-generated method stub
			for (Iterator<Observer> it = observerList.iterator(); it.hasNext();){
				Observer o = it.next();
				o.update(pricesSent);
			}
		}
		
		public void dataChange(String symbol, int num) {
			Pricing pricing = new Pricing();
			ArrayList<PricePoint> prices = new ArrayList<PricePoint>(); 
			prices = pricing.getPriceData(symbol, num);
			
			notifyObservers();
		}
		
		
	}
	
	private class DisplayStock implements Observer {

		@Override
		public void update(ArrayList<PricePoint> pricesSent) {
			// TODO Auto-generated method stub
			System.out.println(pricesSent);
		}
		
	}

	public static final int MAX_PERIODS_WE_CAN_FETCH = 120;
	public static final int SECONDS_PER_PERIOD = 15;
	
	public static PricePoint parsePricePoint(String CSV) {
		SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String[] fields = CSV.split(",");
		if (fields.length < 6) {
			throw new IllegalArgumentException
				("There must be at least 6 comma-separated fields: '" + CSV + "'");
		}
		
		try {
			Timestamp timestamp = new Timestamp(parser.parse(fields[0]).getTime());
			double open = Double.parseDouble(fields[1]);
			double high = Double.parseDouble(fields[2]);
			double low = Double.parseDouble(fields[3]);
			double close = Double.parseDouble(fields[4]);
			int volume = Integer.parseInt(fields[5]);
			
			return new PricePoint(timestamp, open, high, low, close, volume);
		} catch (Exception ex) {
			throw new RuntimeException("Couldn't parse timestamp.", ex);
		}
	}
	
	/**
	 * Requests data from the HTTP service and returns it to the caller.
	 */
	public ArrayList<PricePoint> getPriceData(String symbol, int num) {
		ArrayList<PricePoint> prices = new ArrayList<PricePoint>(); 
		
		try {
			
				String requestURL = "http://will1.conygre.com:8081/prices/" + symbol + "?periods=" + num;
				BufferedReader in = new BufferedReader(new InputStreamReader
						(new URL(requestURL).openStream()));
				in.readLine(); // header ... right? No way that could break ...
				String line;
				while((line = in.readLine()) != null) {
					PricePoint price = Pricing.parsePricePoint(line);
					price.setStock(symbol);
					prices.add(price);
				}
				
			return prices;
			
		} catch (IOException ex) {
			throw new RuntimeException
					("Couldn't retrieve price for " + symbol + ".", ex);
		}
	}
	
	/**
	 * Quick test of the component.
	 */
	public static void main(String[] args) {
		Pricing pricing = new Pricing();
		ArrayList<PricePoint> MRKPrice = new ArrayList<PricePoint>(); 
		MRKPrice = pricing.getPriceData("MRK", 3);
		//System.out.println(MRKPrice);
		
		//DisplayStock ds1 = new DisplayStock();
		
		
		
	}
}
